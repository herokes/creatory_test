import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col, Form, Button, Alert } from 'react-bootstrap';

import styles from './Login.module.scss';

import AuthService from '../../services/AuthService';

export default function Login({ setToken }) {
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();
  const [error, setError] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError(false);
    const authService = new AuthService();
    const res = await authService.loginUser({
      username,
      password,
    });

    if (res.authenticated === true) {
      setToken(res);
    } else {
      setError(true);
    }
  };

  return (
    <div className={styles.login}>
      <Container>
        <Row>
          <Col md={{ span: 6, offset: 3 }}>
            <h1>Login</h1>
            {error && (
              <Alert variant="danger">Invalid Username or Password!</Alert>
            )}

            <Form onSubmit={handleSubmit}>
              <Form.Group controlId="formBasicUsername">
                <Form.Label>Username</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter username"
                  onChange={(e) => setUserName(e.target.value)}
                />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </Form.Group>
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

Login.propTypes = {
  setToken: PropTypes.func.isRequired,
};
