import React, { useEffect, useState } from 'react';
import { Container, Table, Button } from 'react-bootstrap';

import styles from './Dashboard.module.scss';

import DataService from '../../services/DataService';

export default function Dashboard() {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [isFullData, setIsFullData] = useState(false);
  const [offset, setOffset] = useState(0);
  const limit = 20;

  useEffect(() => {
    async function fetchData() {
      const dataService = new DataService();
      setLoading(true);
      const res = await dataService.getData({ limit });
      setLoading(false);
      if (res?.length) {
        setData(res);
        setOffset(res.length);
      }
    }

    fetchData();
  }, []);

  const loadMoreData = async (offset) => {
    const dataService = new DataService();
    setLoading(true);
    const res = await dataService.getData({ limit, offset });
    setLoading(false);
    let newData = [...data];
    if (res?.length) {
      if (res.length < limit - 1) {
        setIsFullData(true);
      }
      newData = data.concat(res);
      setData(newData);
      setOffset(newData.length);
    } else {
      setIsFullData(true);
    }
  };

  return (
    <div className={styles.dashboard}>
      <Container>
        <h1>Dashboard</h1>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
            {data.map((item) => (
              <tr key={item.guid}>
                <td>{item.id}</td>
                <td>{item.firstName}</td>
                <td>{item.lastName}</td>
                <td>{item.email}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        {!isFullData && !loading && (
          <Button
            variant="primary"
            type="button"
            onClick={() => {
              loadMoreData(offset);
            }}
          >
            Load more data
          </Button>
        )}
      </Container>
    </div>
  );
}
