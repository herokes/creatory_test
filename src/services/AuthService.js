class AuthService {
  async loginUser(credentials) {
    return fetch(`${process.env.REACT_APP_API_URL}/auth`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(credentials),
    }).then((data) => data.json());
  }
}

export default AuthService;
