class DataService {
  async getData(options) {
    let params = [];
    for (const key in options) {
      params.push(`${key}=${options[key]}`);
    }
    return fetch(`${process.env.REACT_APP_API_URL}/data?${params.join('&')}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((data) => data.json());
  }
}

export default DataService;
